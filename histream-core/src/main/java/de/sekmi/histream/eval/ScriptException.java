package de.sekmi.histream.eval;

public class ScriptException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ScriptException(Throwable cause){
		super(cause);
	}
	
	public ScriptException(String message){
		super(message);
	}
}
